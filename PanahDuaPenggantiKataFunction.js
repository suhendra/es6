// => bisa menggantikan kata function
// jika argumentnya hanya 1 bisa tanpa tanda kurung ()
// kata return tidak harus digunakan
// ingat ! dengan => kata this. mengacu kepada classnya, sedangkan dg function mengacu ke global windows


class TaskCol {
  constructor(tasks = []){
    this.tasks = tasks;
  }

  log(){
    // this.tasks.forEach(function(task){
    //   return console.log(task)
    // })
    this.tasks.forEach(task => console.log(task));
    // this.tasks.forEach(() => console.log('task')); // Jika tanpa argumen harus ada tanda kurung kosong
  }

}

class Task {}

new TaskCol([new Task, new Task, new Task]).log();

// Contoh lain

let names = [ 'suhendra', 'muhammad', 'naura'];

// lama
// names = names.map(function(name){
//   return name + ' sholeh';
// });
names = names.map(name => `${name} sholeh`);

console.log(names);
