// sekarang kita memasukkan string ke dalam text dengan backtick ``
// ` Halo ${nama}`

name = 'Suhendra Bisa..'
pesan = 'Alhamdulillah..'

template = `
  <div class="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong> ${name} </strong> ${pesan}
  </div>
`;

console.log(template)
