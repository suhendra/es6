// class is first class citizen in javascript, can be pass as argument
// Class sugar, sebenarnya akan di rubah menjadi prototype juga.
// Mengapa harus pake prototype,
// karena kalo langsung dia akan menuhin memory, setiap instancenya juga akan mengandung fungsi itu
// kalo pake prototype, 1 instance akan menggunakan fungsi yang sama.


// before
// function User(username, email)
// {
//   this.username = username;
//   this.email = email;
//   // this.changeEmail = function (newEmail){
//   //   this.email = newEmail;
//   // }
// }

// User.prototype.changeEmail = function (newEmail){
//   this.email = newEmail;
// }

// es6
class User {

  constructor(username, email, name) {
    this.username = username;
    this.email = email;
    this._name = name;
  }
  static register(...args){
    return new User(...args);
  }
  changeEmail(newEmail){
    this.email = newEmail;
  }

  // getter dan setter digunakan jika ingin menambah logic pada variabel,
  // tapi di constructornya tidak boleh pake nama yang sama, contoh ditambah _ (_name)
  get name() {
      return this._name.toUpperCase();
  }

  set name(newName){
      if(newName){
          this._name = newName;
      }
  }
}

// let user = new User('Suhendra', 'idsuhendra@gmail.com', 'Hendra');
let user = User.register('Suhendra', 'idsuhendra@gmail.com', 'Hendra');

console.log(user.name);
user.name = "Suhendra katrali"
console.log(user.name);
console.log(user.username);

function log(strategy){
  strategy.handle();
}

// log(new class {
//   handle(){
//     console.log('log dengan console.');
//   }
// });

class ConsoleLogger {
  handle(){
    console.log('log dengan console.');
  }
}

log(new ConsoleLogger);

