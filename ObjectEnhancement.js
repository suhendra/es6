// Object Shorthand
// Kalo nama variable dan propertynya sama bisa di hapus satu ga perlu di assign lagi

function getPerson() {
  let name = 'John';
  let age = 18;

  // dulu
  // return {
  //   name: name,
  //   age: age,
       // greet: function(){
       //    return `Hello ${this.name}`;
       // }
  // }

  // sekarang
  return { name, age,
    greet(){
        return `Hello ${this.name}`;
    }
   }
}

console.log(getPerson(), getPerson().name, getPerson().age, getPerson().greet());

let data = {
  name: 'Suhendra',
  age: 32,
  results: [ 'foo', 'bar'],
  count: 12
}

// before
// let name = data.name
// let age = data.age
// let results = data.results
// let count = data.count
// console.log(name, age)
// console.log(results, count)

// after
// let { name, age } = data;
// let { results, count } = data;

// console.log(name, age)
// console.log(results, count)

function getData({results, count}){ // sekarang bisa dengan begini
// function getData(data){
  // let results = data.results;
  // let count = data.count;
  console.log(results, count);
}

getData(data);
