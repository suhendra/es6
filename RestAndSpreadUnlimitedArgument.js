// namanya rest (unlimites arguments)

// Lama
// function sum(...nums){
//   console.log(nums);
//   return nums.reduce(function(prev, next){
//     return prev + next;
//   })
// }

// Baru
function sum(...nums){
  return nums.reduce((prev, next) => prev + next);
}

hasil = sum(1,2,3,4,5);

console.log(hasil);

// kebalikan dari rest namanya spread
// merubah array jadi arguments terpisah

data = [2,3,4,5];
console.log(data, 'ini masih array');
console.log(...data, 'dari array jadi terpisah');
console.log(sum(...data));
