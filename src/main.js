// sekarang kita import dan export file dari sebuah class,
// kalo dulu harus masuk script tag semua atau pakai tools seperti CommonJS, AMD, UMD
// Kita bisa export dan import variable, class, fungsi, dsb,
// yang penting kata kunci yang di import adalah kata kunci yang sudah di export
// dengan kata export default maka kita bisa import tanpa tanda {}

import NamaClass, { data, fungsi } from './NamaModuleFile';

let contoh = new NamaClass('Suhendra');
contoh.dump();
console.log(data);

fungsi();
